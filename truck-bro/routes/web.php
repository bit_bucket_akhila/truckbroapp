<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Auth::routes();
Route::redirect('/', '/login');

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login-form');
//Route::get('/home', 'HomeController@index')->name('home');

Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::post('/verify-otp', 'Auth\LoginController@verifyOtp')->name('verify-otp');
Route::post('/client-basic-registration', 'Auth\LoginController@clientBasicReg')->name('client-registration');


Route::get('dashboard', 'DashboardController@getDashboard');
Route::get('profile', 'ProfileController@createProfile');
Route::post('basic-details', 'ProfileController@storeBasicDetails');
Route::get('city-by-state-id/{state_id}', 'ProfileController@cityByStateId');
Route::post('add-hub-to-a-client', 'ProfileController@addHubToaClient');
Route::post('add-gst-info', 'ProfileController@addGSTInfo');
Route::get('feedback', 'FeedbackController@create');
Route::post('feedback', 'FeedbackController@store');
Route::get('booking/create', 'BookingController@create');
