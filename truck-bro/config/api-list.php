<?php
define('BASE_URL', 'http://bts.g10solution.com:8900/');

define('GET_COUNTRIES', 'api/allcountries');
define('CLIENT_TYPES', 'api/allclient_type');
define('GET_STATES', 'api/allstates');
define('GET_STATES_BY_COUNTRY', 'api/allstatesbyCountry');
define('ALL_HUBS', 'api/allhubs');


define('FIREBASE_AUTHORIZATION', 'api/firebase/auth');
define('CLIENT_BASIC_REG', 'api/clientbasicreg');
define('UPDATE_GEO_LOC', 'api/updateClientGeo');
define('UPDATE_CLIENT_BASIC_REG', 'api/updateclibr');
define('GET_CLIENT_DATA', 'api/getclientdata');
define('ALL_HUBS_BANK_DETAIL', 'api/allclihubbankdetail');
define('ADD_HUB', 'api/clihubbankdetail');
define('ADD_BANK_INFO', 'api/clitaxbankdetail');
define('POST_FEEDBACK', 'api/addfeedback');
define('GET_FEEDBACK', 'api/getownfeedback');

