@extends('layouts.master')
@section('page_title','Booking')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">New Booking
                    </h4>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div id="smartwizard" class="form-wizard order-create">
                                <ul class="nav nav-wizard">
                                    <li><a class="nav-link" href="#wizard_Service">
                                            <span>1</span>
                                        </a></li>
                                    <li><a class="nav-link" href="#wizard_Time">
                                            <span>2</span>
                                        </a></li>
                                    <li><a class="nav-link" href="#wizard_Details">
                                            <span>3</span>
                                        </a></li>
                                </ul>
                                <div class="tab-content">
                                    <form action="{{url('submit-enquiry')}}" method="POST">
                                        <div id="wizard_Service" class="tab-pane" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-12 ">
                                                    <div class="form-group">
                                                        <label class="text-label">Select Truck Type , Qty,Target
                                                            Price</label>
                                                        <input type="text" name="firstName" class="form-control"
                                                               placeholder="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">Select pickup location</label>
                                                        <select class="form-control">
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">Select dropping
                                                            location</label>
                                                        <select class="form-control">
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">Pickup Date</label>
                                                        <input type="text" class="form-control"
                                                               name="pickup_date">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">Pickup Time</label>
                                                        <input type="text" class="form-control"
                                                               name="pickup_time">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="wizard_Time" class="tab-pane" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                        <label class="text-label">Send Enquiry To*</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">Brokers
                                                            at booking Hub</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">Brokers
                                                            at Pickup Location</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">My
                                                            Preferred Brokers</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">Truck
                                                            Freelancers</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12">
                                                    <div class="form-group">
                                                            <textarea class="form-control" rows="4"
                                                                      placeholder="+ Also send to these people/enter mobile # manually"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">My Customer Name *</label>
                                                        <input type="text" class="form-control" id="cname"
                                                               placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">Mobile # of the
                                                            customer*</label>
                                                        <input type="text" name="place" class="form-control"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <label class="text-label">Pickup Address*</label>
                                                        <input type="text" name="phoneNumber"
                                                               class="form-control"
                                                               placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Delivery Address*</label>
                                                        <input type="text" name="place" class="form-control"
                                                               required>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        <div id="wizard_Details" class="tab-pane" role="tabpanel">
                                            <div class="row">
                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Select Truck Type , Qty,Target
                                                            Price</label>
                                                        <input type="text" name="firstName" class="form-control"
                                                               placeholder="" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Select pickup location</label>
                                                        <select class="form-control">
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Select dropping
                                                            location</label>
                                                        <select class="form-control">
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Pickup Date</label>
                                                        <input type="text" class="form-control"
                                                               name="pickup_date">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Pickup Time</label>
                                                        <input type="text" class="form-control"
                                                               name="pickup_time">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Send Enquiry To*</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">Brokers
                                                            at booking Hub</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">Brokers
                                                            at Pickup Location</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">My
                                                            Preferred Brokers</label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="custom-control custom-checkbox mb-3">
                                                        <input type="checkbox" class="custom-control-input"
                                                               id="customCheckBox1" required="">
                                                        <label class="custom-control-label"
                                                               for="customCheckBox1">Truck
                                                            Freelancers</label>
                                                    </div>
                                                </div>

                                                <div class="col-lg-12 mb-2">
                                                    <div class="form-group">
                                                            <textarea class="form-control" rows="4"
                                                                      placeholder="+ Also send to these people/enter mobile # manually"></textarea>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">My Customer Name *</label>
                                                        <input type="text" class="form-control" id="emial1"
                                                               placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Mobile # of the
                                                            customer*</label>
                                                        <input type="text" name="place" class="form-control"
                                                               required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Pickup Address* </label>
                                                        <input type="text" name="phoneNumber"
                                                               class="form-control"
                                                               placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 mb-2">
                                                    <div class="form-group">
                                                        <label class="text-label">Delivery Address*</label>
                                                        <input type="text" name="place" class="form-control"
                                                               required>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row justify-content-center">
                                                <div class="col-sm-12 ">
                                                    <div class="form-group">
                                                        <input type="submit" value="Send Enquiry"
                                                               class="btn btn-secondary">
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('scripts')
    <!-- Datatable -->




    <!-- Form Steps -->


    <script type="text/javascript">

        $(document).ready(function () {
            $('#smartwizard').smartWizard();
            //    $('#example').DataTable();
        });
    </script>
@endsection

