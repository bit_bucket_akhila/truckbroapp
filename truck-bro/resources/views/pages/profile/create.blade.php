@extends('layouts.master')
@section('page_title','Profile')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">My Profile
                    </h4>
                </div>

                <div class="card-body">
                    <!-- Nav tabs -->
                    <div class="custom-tab-1">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#home1"><i
                                        class="la la-user-tag mr-2"></i> Basic Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " data-toggle="tab" href="#profile1"><i
                                        class="la la-map-marker mr-2"></i> Hub/District You Operate</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#contact1"><i
                                        class="la la-bookmark-o  mr-2"></i>
                                    GST Info</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade active show" id="home1" role="tabpanel">
                                <div class="pt-4">
                                    <div class="card">

                                        <div class="card-body">
                                            <div class="form-validation">
                                                <form class="form-valide" action="#" id="basic-details"
                                                      novalidate="novalidate">
                                                    {{csrf_field()}}
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="row">
                                                                <div class="form-group col-12">
                                                                    <label> <span
                                                                            class="text-danger">*</span>I am
                                                                    </label>
                                                                    <input type="text" class="form-control"
                                                                           name=""
                                                                           value=""
                                                                           autocomplete="off"
                                                                           placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="col-form-label"> <span
                                                                                class="text-danger">*</span>State
                                                                            I am based
                                                                        </label>
                                                                        <select class="form-control" name="state_id"
                                                                                id="id_for_state">
                                                                            <option>State</option>
                                                                            @foreach($states as $state)
                                                                                <option
                                                                                    value="{{$state['id']}}"
                                                                                    @if(isset($client_data['cr_state'])==$state['id']) selected @endif>{{$state['name_eng']}}</option>
                                                                            @endforeach
                                                                        </select>

                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group ">
                                                                        <label> <span
                                                                                class="text-danger">*</span>District/Hub
                                                                            you based at
                                                                        </label>
                                                                        <select class="form-control"
                                                                                name="city_id" id="id_for_city">
                                                                            <option>Hub</option>
                                                                            @foreach($allhubs as $allhub)
                                                                                <option
                                                                                    value="{{$allhub['id']}}"
                                                                                    @if(isset($client_data['cr_city'])==$allhub['id'] ) selected @endif>{{$allhub['name_eng']}}</option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row">

                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group ">
                                                                        <label for="val-email">Google Map
                                                                        </label>
                                                                        <input type="text" class="form-control"
                                                                               name="" placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group  col-12">
                                                                        <label>  <span
                                                                                class="text-danger">*</span>Company
                                                                            Name</label>
                                                                        <input type="text" class="form-control"
                                                                               value="@if(isset($client_data['cr_biz_name'])){{$client_data['cr_biz_name']}} @endif"
                                                                               name="business_name" autocomplete="off"
                                                                               placeholder="">
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Email
                                                                        </label>
                                                                        <input type="text" class="form-control"
                                                                               name="email" autocomplete="off"
                                                                               value="@if(isset($client_data['cr_primary_email'])){{$client_data['cr_primary_email']}} @endif"
                                                                               placeholder="">
                                                                    </div>
                                                                </div>
                                                                <div class="col-12 col-sm-6">
                                                                    <div class="form-group">
                                                                        <label>Contact Person
                                                                        </label>
                                                                        <input type="text" class="form-control"
                                                                               autocomplete="off" name="contact_person"
                                                                               value="@if(isset($client_data['cr_contact_person'])){{$client_data['cr_contact_person']}} @endif"
                                                                               placeholder="">

                                                                        <input type="hidden" class="form-control"
                                                                               autocomplete="off" name="id"
                                                                               value="@if(isset($client_data['id'])){{$client_data['id']}} @endif"
                                                                               placeholder="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="row justify-content-center">
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-secondary">Save
                                                                        and Proceed
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade " id="profile1">
                                <div class="pt-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-12">
                                                    <form action="{{url('add-hub-to-a-client')}}" method="POST">
                                                        {{csrf_field()}}
                                                        <div class="row">
                                                            <div class="col-sm-8">
                                                                <div class="form-group ">
                                                                    <label for="val-email"> <span
                                                                            class="text-danger">*</span>District/Hub
                                                                        you Operate
                                                                    </label>
                                                                    <select name="chb_hub_id" class="form-control">
                                                                        <option>Hubs</option>
                                                                        @foreach($allhubs as $allhub)
                                                                            <option
                                                                                value="{{$allhub['id']}}">{{$allhub['name_eng']}}</option>
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <div class="form-group pt-4">
                                                                    <button type="submit" class="btn btn-success">Add
                                                                        District/Hub You Operate
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-center">
                                                            <div class="col-sm-6 col-12">

                                                            </div>
                                                            <div class="col-sm-6 col-12">
                                                                <div class="form-group">
                                                                    <button type="button" disabled
                                                                            class="btn btn-secondary">Save
                                                                        and Proceed
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card">
                                        <div class="card-header">Operating Dist/Hubs</div>
                                        <div class="card-body">
                                            <div class="card-body">
                                                <div class="table-responsive">
                                                    <table class="table header-border table-responsive-sm">
                                                        <thead>
                                                        <tr>
                                                            <th>State</th>
                                                            <th>District</th>
                                                            <th></th>

                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($operating_hubs as $operating_hub)
                                                            <tr>
                                                                <td>{{$operating_hub['hubs']['state_id']}}
                                                                </td>
                                                                <td>{{$operating_hub['hubs']['hub_name_eng']}}</td>
                                                                <td>
                                                                    <button type="button" class="btn btn-danger btn-sm">
                                                                        <i
                                                                            class="fa fa-close"></i>
                                                                    </button>
                                                                </td>

                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="contact1">
                                <div class="pt-4">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="{{url('add-gst-info')}}" method="POST">
                                                {{csrf_field()}}
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>State</label>
                                                                    <select class="form-control" name="chb_state_id">
                                                                        <option></option>
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>GST No</label>
                                                                    <input type="text" autocomplete="off"
                                                                           name="chb_gst_no"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-12 col-sm-6">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>Upload GST Ref
                                                                        Doc</label>
                                                                    <input type="file" name="chb_tax_regn_document"
                                                                           class="form-control"></div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>Bank
                                                                        Name</label>
                                                                    <input type="text" autocomplete="off"
                                                                           name="chb_bank_name"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>IFSC
                                                                        Code</label>
                                                                    <input type="text" autocomplete="off"
                                                                           name="chb_ifsc_code"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>Bank A/c
                                                                        Name</label>
                                                                    <input type="text" autocomplete="off"
                                                                           class="form-control" name="chb_account_name">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group">
                                                                    <label><span
                                                                            class="text-danger">*</span>Bank A/c
                                                                        No</label>
                                                                    <input type="text" autocomplete="off"
                                                                           name="chb_account_number"
                                                                           class="form-control">
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="form-group"><span
                                                                        class="text-danger">*</span><label>Swift</label>
                                                                    <input type="text" autocomplete="off"
                                                                           name="chb_swift_iban"
                                                                           class="form-control"></div>
                                                            </div>
                                                        </div>
                                                        <div class="row justify-content-center">
                                                            <div class="col-sm-6 col-12">
                                                                <div class="form-group">
                                                                    <button type="submit" class="btn btn-success">+
                                                                        GST/Bank
                                                                        Info
                                                                    </button>
                                                                </div>
                                                            </div>
                                                            <div class="col-sm-6 col-12">
                                                                <div class="form-group">
                                                                    <button type="button" disabled
                                                                            class="btn btn-secondary">Save
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script type="text/javascript">

        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#basic-details').on('submit', function (event) {
                event.preventDefault();
                // $('#state_error').text('');
                // $('#city_error').text('');
                // $('#company_name_error').text('');
                // $('#email_error').text('');
                // $('#contact_person_error').text('');
                var formData = $("#basic-details").serialize();
                $.ajax({
                    url: "{{url('basic-details')}}",
                    type: "POST",
                    dataType: 'json',
                    data: formData,
                    success: function (response) {
                        console.log(response);
                        if (response && response.status_code == 200) {
                            $('#myTab a[href="#profile1"]').tab('show');
                        }
                    },
                    error: function (response) {
                        // $('#state_error').text(response.responseJSON.errors.state_id);
                        // $('#city_error').text(response.responseJSON.errors.city_id);
                        // $('#company_name_error').text(response.responseJSON.errors.business_name);
                        // $('#email_error').text(response.responseJSON.errors.email);
                        // $('#contact_person_error').text(response.responseJSON.errors.contact_person);
                    }
                });
            });


            // $('#action').click(function (e) {
            //     e.preventDefault();
            //     $('#myTab a[href="#profile1"]').tab('show');
            // })


        });
    </script>
@endsection

