@extends('layouts.master')
@section('page_title','Profile')
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Complaints and Suggestions
                    </h4>
                    <button type="button" class="btn btn-primary mb-2" data-toggle="modal"
                            data-target="#exampleModalCenter">Send Feedback
                    </button>
                </div>

                <div class="card-body">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table  table-bordered  table-responsive-md">
                                <thead>
                                <tr>
                                    <th class="width80"><strong>#</strong></th>
                                    <th><strong>Date</strong></th>
                                    <th><strong><span class="text-success">Remarks from You</span><br>
                                            <span class="text-danger"> Response from support</span></strong></th>
                                    <th><strong><span class="text-success">Your Status</span><br>
                                            <span class="text-danger">Support Status</span></strong></th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(!empty($feedback['data']))
                                    @foreach($feedback['data'] as $index=>$data)
                                        <tr>
                                            <td>{{$index+1}}</td>
                                            <td>{{ \Carbon\Carbon::parse($data['created_on'])->format('d/m/Y')}}</td>
                                            <td><span class="text-success">{{ $data['sc_detail']}}</span></td>

                                            <td><span class="text-success">{{ $data['feedbackStatus']}}</span>
                                            </td>


                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="exampleModalCenter">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Your Feedback</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{url('feedback')}}">
                    {{csrf_field()}}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group mb-0">
                                    <label class="radio-inline mr-3"><input type="radio" checked name="sc_type"
                                                                            value="S">
                                        Suggestion</label>
                                    <label class="radio-inline mr-3"><input type="radio" name="sc_type" value="C">
                                        Complaint</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Type here....." rows="4"
                                              name="sc_detail" id="comment"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger light" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Send Suggestion</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <!-- Datatable -->


    <script type="text/javascript">

        $(document).ready(function () {
            //    $('#example').DataTable();
        });
    </script>
@endsection

