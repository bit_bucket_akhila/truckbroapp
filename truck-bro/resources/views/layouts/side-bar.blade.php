<div class="dlabnav">
    <div class="dlabnav-scroll">
        <ul class="metismenu" id="menu">
            <li><a class="has-arrow ai-icon" href="{{url('dashboard')}}" aria-expanded="false">
                    <i class="flaticon-381-networking"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                {{--                <ul aria-expanded="false">--}}
                {{--                    <li><a href="#">Dashboard</a></li>--}}
                {{--                    <li><a href="#">Projects</a></li>--}}
                {{--                    <li><a href="#">Contacts</a></li>--}}
                {{--                    <li><a href="#">Kanban</a></li>--}}
                {{--                    <li><a href="#">Calendar</a></li>--}}
                {{--                    <li><a href="#">Messages</a></li>--}}
                {{--                </ul>--}}
            </li>
            <li><a class="has-arrow ai-icon" href="{{url('profile')}}" aria-expanded="false">
                    <i class="flaticon-381-television"></i>
                    <span class="nav-text">Profile</span>
                </a>

            </li>
            <li><a class="has-arrow ai-icon" href="{{url('feedback')}}" aria-expanded="false">
                    <i class="flaticon-381-controls-3"></i>
                    <span class="nav-text">Feedback</span>
                </a>

            </li>
            <li><a class="has-arrow ai-icon" href="{{url('booking/create')}}" aria-expanded="false">
                    <i class="flaticon-381-internet"></i>
                    <span class="nav-text">Booking</span>
                </a>

            </li>
        </ul>

        <div class="copyright">
            <p><strong>TruckBro</strong> © {{date('Y')}} All Rights Reserved</p>
            <p>Made with <span class="heart"></span> by DexignLab</p>
        </div>
    </div>
</div>
