<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>TruckBro | @yield('page_title')</title>
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('theme/images/favicon.png')}}">
    @include('layouts.styles')
</head>
<body>

<!--*******************
    Preloader start
********************-->
<div id="preloader">
    <div class="sk-three-bounce">
        <div class="sk-child sk-bounce1"></div>
        <div class="sk-child sk-bounce2"></div>
        <div class="sk-child sk-bounce3"></div>
    </div>
</div>
<div id="main-wrapper">

    @include('layouts.header')
    @include('layouts.side-bar')
    <div class="content-body">
        <div class="container-fluid">
            @yield('content')
        </div>
    </div>
    @include('layouts.footer')
</div>

@include('layouts.scripts')
@yield('scripts')

</body>
</html>
