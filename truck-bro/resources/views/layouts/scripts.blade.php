{{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>--}}
<script src="{{url('theme/vendor/jquery/jquery.min.js')}}"></script>


<!-- Required vendors -->
<script src="{{url('theme/vendor/global/global.min.js')}}"></script>
<script src="{{url('theme/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script src="{{url('theme/vendor/chart.js/Chart.bundle.min.js')}}"></script>
<script src="{{url('theme/js/custom.min.js')}}"></script>
<script src="{{url('theme/js/dlabnav-init.js')}}"></script>
<script src="{{url('theme/vendor/owl-carousel/owl.carousel.js')}}"></script>
<script src="{{url('theme/vendor/peity/jquery.peity.min.js')}}"></script>
<script src="{{url('theme/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{url('theme/js/plugins-init/jquery.validate-init.js')}}"></script>
<script src="{{url('theme/vendor/jquery-smartwizard/dist/js/jquery.smartWizard.js')}}"></script>


<!-- Dashboard 1 -->
{{--<script src="{{url('theme/js/dashboard/dashboard-1.js')}}"></script>--}}
