<!DOCTYPE html>
<html lang="en" class="h-100">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title>Vora - Saas Bootstrap Admin Dashboard</title>
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="./images/favicon.png">
    <link href="{{url('theme/css/style.css')}}" rel="stylesheet">
    <style type="text/css">
        .error {
            color: red;
        }
    </style>

</head>

<body class="h-100">
<div class="authincation h-100">
    <div class="container h-100">
        <div class="row justify-content-center h-100 align-items-center">
            <div class="col-md-6">
                <div class="authincation-content">
                    <div class="row no-gutters">
                        <div class="col-xl-12">
                            <div class="auth-form">
                                <div class="text-center mb-3">
                                    <a href="index.html"><img src="{{url('theme/images/logo-full.png')}}" alt=""></a>
                                </div>
                                <h4 class="text-center mb-4 text-white">Sign in your account</h4>
                                <div class="col-12">

                                    <form action="/" class="mt-4" id='loginForm'>
                                        {{-- Form::open(array('url' => '/','class' => 'mt-4')) --}}
                                        <div class="form-group">
                                            <label class="mb-1 text-white"><strong>I am a <span
                                                        class="text-danger">*</span></strong></label>
                                            <select class="form-control" name="client_type" id="id_for_client_type">
                                                @foreach($client_types as $client_type)
                                                    <option
                                                        value="{{$client_type['id']}}">{{$client_type['description_eng']}}</option>
                                                @endforeach
                                            </select>
                                            @error('client_type')
                                            <p class="text-white">{{ $message }} *</p>
                                            @enderror

                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="text-white">Country</label>
                                            <select class="form-control" id="isdcode" name='country' required="">
                                                @foreach($countries as $country)
                                                    <option
                                                        value="{{$country['isd']}}"
                                                        data-country_id="{{$country['id']}}">{{$country['name_eng']}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="pwd" class="text-white">Mobile</label>
                                            <input type="tel" class="form-control" id="phoneNumber" autocomplete="off"
                                                   placeholder=""
                                                   name="contact_number" required="">
                                        </div>
                                        <!--                    <label for="pwd">Password</label>
                                                            <div class="form-group input-group">
                                                                <input type="password" class="form-control" id="pwd" placeholder="" name="pswd" data-toggle="password" required="">
                                                                <div class="input-group-append">
                                                                    <span class="input-group-text">Show</span>
                                                                </div>
                                                            </div>
                                                            <label id="pwd-error" class="error" for="pwd"></label>
                                                            <div class="form-group form-check">
                                                                <label class="form-check-label container-box">
                                                                    <input class="form-check-input" type="checkbox" name="remember"> Reminder me
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>-->

                                        <!-- Add a container for reCaptcha -->
                                        <div id="recaptcha-container"></div>

                                        <div class="validate_otp">
                                            <label for="pwd" class="text-white">Enter OTP</label>
                                            <div class="form-group input-group">
                                                <input type="text" class="form-control" id="code" placeholder=""
                                                       name="pswd" data-toggle="Send OTP">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">OTP</span>
                                                </div>
                                            </div>
                                            <div class="text-center">

                                                <button type="button" class="btn btn-primary text-primary  bg-white"
                                                        data-toggle="modal"
                                                        id="confirm-code" onclick="submitPhoneNumberAuthCode()">Validate
                                                    OTP
                                                </button>
                                            </div>
                                            <div class="pull-right">
                                                <p class="text-white">Not Received ?</p>
                                                <a href="javascript:void(0)" onclick="submitPhoneNumberAuth()"
                                                   class="text-white"> Resend
                                                    OTP</a>
                                            </div>
                                        </div>
                                        <label id="otp-error" class="text-danger" for="pwd"></label>
                                        <div class="text-center">
                                            <button type="button" id="sign-in-button"
                                                    class="btn btn-primary text-primary  bg-white"
                                                    onclick="submitPhoneNumberAuth()">Send OTP
                                            </button>
                                        </div>
                                        <!--                    <hr>
                                                            <div class="resend">
                                                                <a href="#">Forgot Password?</a>
                                                            </div>-->
                                        {{-- Form::close() --}}
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--**********************************
    Scripts
***********************************-->
<!-- Required vendors -->
<script src="{{url('theme/vendor/global/global.min.js')}}"></script>
<script src="{{url('theme/vendor/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script src="{{url('theme/js/custom.min.js')}}"></script>
<script src="{{url('theme/js/dlabnav-init.js')}}"></script>
<script src="{{url('theme/vendor/jquery-validation/jquery.validate.min.js')}}"></script>
<!-- Form validate init -->

{{--<script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>--}}
<!-- Add the latest firebase dependecies from CDN -->
<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/6.3.3/firebase-auth.js"></script>
<script type="text/javascript">


    $("#loginForm").validate({
        rules: {
            mobile: {
                required: true,
                number: true,
                rangelength: [10, 10],

            }
        }
    });
    $('.validate_otp').hide();
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyAkfK2CHFvV13NZtc7sFZjxgCzl4FJOJaQ",
        authDomain: "truckbro-52040.firebaseapp.com",
        databaseURL: "https://truckbro-52040.firebaseio.com",
        projectId: "truckbro-52040",
        storageBucket: "truckbro-52040.appspot.com",
        messagingSenderId: "524884118776",
        appId: "1:524884118776:web:0b70111df6bfafa0127f0d",
        measurementId: "G-HFDDPGYSS3"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);

    // Create a Recaptcha verifier instance globally
    // Calls submitPhoneNumberAuth() when the captcha is verified
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
        "sign-in-button",
        {
            size: "invisible",
            callback: function (response) {
                submitPhoneNumberAuth();
            }
        }
    );
    // This function runs when the 'sign-in-button' is clicked
    // Takes the value from the 'phoneNumber' input and sends SMS to that phone number
    function submitPhoneNumberAuth() {
        $('#loginForm').validate();
        if ($('#loginForm').valid()) {
            $('#sign-in-button').attr('disabled', 'disabled');
            var phoneNumber = document.getElementById("isdcode").value + document.getElementById("phoneNumber").value;
            var appVerifier = window.recaptchaVerifier;

            firebase
                .auth()
                .signInWithPhoneNumber(phoneNumber, appVerifier)
                .then(function (confirmationResult) {
                    window.confirmationResult = confirmationResult;
                    $('#sign-in-button').hide();
                    $('.validate_otp').show();
                    $('#otp-error').hide();
                })
                .catch(function (error) {
                    alert(error);
                    $('#otp-error').text(error).show();
                    console.log(error);
                });
        }
    }

    // This function runs when the 'confirm-code' button is clicked
    // Takes the value from the 'code' input and submits the code to verify the phone number
    // Return a user object if the authentication was successful, and auth is complete
    function submitPhoneNumberAuthCode() {
        var code = document.getElementById("code").value;
        confirmationResult
            .confirm(code)
            .then(function (result) {
                console.log(result);
                var user = result.user;
                console.log(user.ra);
                loginSuccess(user.ra);
                $('#otp-error').hide
            })
            .catch(function (error) {
                $('#otp-error').text(error).show();
                console.log(error);
            });
    }

    function loginSuccess(JWTtoken) {
        console.log(JWTtoken);
        var phoneNumber = document.getElementById("isdcode").value + document.getElementById("phoneNumber").value;
        var country_id = $('#isdcode').find(':selected').data('country_id');
        $.ajax({
            type: 'GET',
            url: "{{ url('client-basic-registration')}}",
            async: false,
            data: {
                token: JWTtoken,
                client_type: $('#id_for_client_type').val(),
                country_id: country_id,
                mobile: phoneNumber
            },
            success: function (result) {
                console.log(result);
                if (result.status_code == 200) {
                    window.location = "{{ url('/dashboard')}}";
                } else {
                    $('#otp-error').text(result.message);
                    console.log(result.message);
                }
            }
        });
    }

    //This function runs everytime the auth state changes. Use to verify if the user is logged in
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {

            loginSuccess(user.ra);
            console.log("USER LOGGED IN");
        } else {
            // No user is signed in.
            //alert("USER NOT LOGGED IN");
            //$('#otp-error').text("USER NOT LOGGED IN");
            console.log("USER NOT LOGGED IN");
        }
    });
</script>
</body>

</html>
