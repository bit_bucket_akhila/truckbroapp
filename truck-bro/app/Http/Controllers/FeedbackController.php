<?php

namespace App\Http\Controllers;

use App\Application\ApiCall;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{
    public $apiObj;

    public function __construct()
    {
        $this->apiObj = new ApiCall();
    }

    public function create()
    {
        $feedback = $this->apiObj->getFeedback();
        return view('pages.feedback.index', compact('feedback'));
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $response = $this->apiObj->postFeedback($input);
        $res = json_decode($response, true);
        if ($res && $res['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            return back();
        } else {
            return back();
        }
    }
}
