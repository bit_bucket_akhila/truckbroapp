<?php

namespace App\Http\Controllers\Auth;

use App\Application\ApiCall;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    public $apiCallObj;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->apiCallObj = new ApiCall();
    }


    public function showLoginForm()
    {
        $countries = $this->apiCallObj->getCountries();
        $client_types = $this->apiCallObj->clientTypes();
        return view('auth.login', compact('countries', 'client_types'));
    }
}
