<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Application\ApiCall;

class ProfileController extends Controller
{
//TODO::Validation
    public $apiObj;

    public function __construct()
    {
        $this->apiObj = new ApiCall();
        $this->apiObj->checkAuth();
    }

    public function createProfile()
    {
        $states = $this->apiObj->getStates();
        $allhubs = $this->apiObj->getAllHubs();
        $operating_hubs = [];
        $states_by_country = [];
        $allhubs = [];
        $operating_hubs = $this->apiObj->operatingHubs();
        $client_data = $this->apiObj->getClientData();
        if (!empty($client_data)) {
            $states_by_country = $this->apiObj->stateByCountry($client_data['cr_country']);
        }
        return view('pages.profile.create', compact('states', 'allhubs', 'operating_hubs', 'client_data', 'states_by_country'));
    }

    public function storeBasicDetails(Request $request)
    {
        $regResponse = $this->apiObj->clientBasicUpdate($request->all());
        $regReg = json_decode($regResponse, true);
        if ($regReg && $regReg['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            return response()->json($regReg);
        } else {
            // return redirect('profile');
        }
    }

    public function addHubToaClient(Request $request)
    {
        //TODO::Validation
        $hubResponse = $this->apiObj->addClientHub($request->all());
        $regReg = json_decode($hubResponse, true);
        if ($regReg && $regReg['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            return redirect('profile');
            return response()->json($regReg);
        } else {
            return redirect('profile');
        }
    }

    public function addGSTInfo(Request $request)
    {

        //TODO::Validation

//        $this->validate($request, [
//                'chb_state_id' => 'required',
//                'chb_gst_no' => 'required',
//                'chb_tax_regn_document' => 'required',
//                'chb_bank_name' => 'required',
//                'chb_account_name' => 'required',
//                'chb_account_number' => 'required',
//                'chb_ifsc_code' => 'required',
//                'chb_swift_iban' => 'required',
//            ]
//        );


        $gstResponse = $this->apiObj->addGSTinfo($request->all());
        $gstRes = json_decode($gstResponse, true);
        if ($gstRes && $gstRes['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            //  return redirect('profile');
            return response()->json($gstRes);
        } else {
            return redirect('profile');
        }
    }
}
