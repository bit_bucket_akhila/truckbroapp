<?php

namespace App\Http\Controllers;

use App\Application\ApiCall;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public $apiObj;

    public function __construct()
    {
        $this->apiObj = new ApiCall();
        $this->apiObj->checkAuth();
    }

    public function getDashboard()
    {
        return view('pages.dashboard');
    }
}
