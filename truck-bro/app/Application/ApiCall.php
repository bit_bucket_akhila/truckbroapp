<?php


namespace App\Application;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\RequestException;

class ApiCall
{
    const RESPONSE_SUCCESS = 200;

    function setToken($token)
    {
        if (isset($_COOKIE['auth_token'])) {
            $_COOKIE['auth_token'];
        } else {
            setcookie('auth_token', $token, time() + 1800);
        }
    }

    function getToken()
    {
        return $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9idHMuZzEwc29sdXRpb24uY29tOjg5MDBcL2FwaVwvZmlyZWJhc2VcL2F1dGgiLCJpYXQiOjE2MTQxNjc1NDYsImV4cCI6MTYxNjc5NTU0NiwibmJmIjoxNjE0MTY3NTQ2LCJqdGkiOiI3YzNhT3hhd0RaZTVlNk9TIiwic3ViIjo2MSwicHJ2IjoiMjNiZDVjODk0OWY2MDBhZGIzOWU3MDFjNDAwODcyZGI3YTU5NzZmNyIsImlkIjo2MSwiY3JfaWQiOjk1fQ.o_EFDicT8WrGy0HUvLDBoZ-RzHfIW5lQfKXagm-B720";
        if (isset($_COOKIE["auth_token"])) {
            return $_COOKIE["auth_token"];
        }
    }

    function checkAuth()
    {
        if (!$this->getToken()) {
            Redirect::to('login')->send();
        }
    }

    public function getData($url)
    {
        $url = BASE_URL . $url;
        $response = Http::get($url);
        return $response;
    }

    function clientTypes()
    {
        $client_types = [];
        $url = CLIENT_TYPES;
        $clientTypes = $this->getGuzzleRequest($url);
        $clientTypes = json_decode($clientTypes, true);
        if ($clientTypes['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            $client_types = $clientTypes['data'];
        }
        return $client_types;
    }

    function getCountries()
    {
        $countries = [];
        $url = GET_COUNTRIES;
        $countryList = $this->getGuzzleRequest($url);
        $countryList = json_decode($countryList, true);
        if ($countryList['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            $countries = $countryList['data'];
        }
        return $countries;
    }

    function fireBaseAuth($token)
    {
        $url = BASE_URL . '/' . FIREBASE_AUTHORIZATION;
        return Http::withToken($token)->get($url);
    }

    function ClientBasicRegistration($data)
    {
        Log::info($data);
        $url = CLIENT_BASIC_REG;
        $data = [
            'cr_client_type' => $data['client_type'],
            'cr_country' => $data['country_id'],
            'cr_mobile' => $data['mobile']];

        return $this->httpWithTokenPost($url, $data);
    }

    function httpWithTokenGet($url)
    {
        $url = BASE_URL . $url;
        $token = $this->getToken();
        return $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ])->get($url);
        return $response = Http::withToken($token)->get($url);
    }

    function httpWithTokenPost($url, $data)
    {
        $url = BASE_URL . $url;
        $token = $this->getToken();
        return $response = Http::withToken($token)->post($url, $data);
    }

    function getStates()
    {
        $states = [];
        $url = GET_STATES;
        $stateList = $this->getGuzzleRequest($url);
        $stateList = json_decode($stateList, true);
        if ($stateList['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            $states = $stateList['data'];
        }
        return $states;
    }

    function getAllHubs()
    {
        $hubs = [];
        $url = ALL_HUBS;
        $hubsList = $this->getGuzzleRequest($url);
        $hubsList = json_decode($hubsList, true);
        if ($hubsList && $hubsList['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            $hubs = $hubsList['data'];
        }
        return $hubs;
    }

    function operatingHubs()
    {
        $hubs = [];
        $url = ALL_HUBS_BANK_DETAIL;
        $hubsList = $this->getGuzzleRequestWithToken($url);
        $hubsList = json_decode($hubsList, true);
        if (!empty($hubsList)) {
            if ($hubsList && $hubsList['status_code'] == ApiCall::RESPONSE_SUCCESS) {
                if (!empty($hubsList['data'])) {
                    $hubs = $hubsList['data'];
                }
            }
        }
        return $hubs;
    }

    function clientBasicUpdate($data)
    {
        Log::info($data);
        $url = UPDATE_CLIENT_BASIC_REG;
        $data = ['id' => $data['id'],
            'cr_contact_person' => $data['contact_person'],
            "cr_biz_name" => $data['business_name'],
            "cr_state" => $data['state_id'],
            "cr_city" => $data['city_id'],
            //    "cr_regd_address" => $data['regd_address'],
            "cr_primary_email" => $data['email']
        ];
        return $this->httpWithTokenPost($url, $data);

    }


    public function addClientHub($data)
    {

        $url = ADD_HUB;
        $data = ['chb_hub_id' => $data['chb_hub_id']];
        return $this->httpWithTokenPost($url, $data);
    }

    function stateByCountry($countryId)
    {
        $states = [];
        $url = GET_STATES_BY_COUNTRY . '/' . $countryId;
        $stateList = $this->getGuzzleRequest($url);
        return $stateList = json_decode($stateList, true);
        if ($stateList['status_code'] == ApiCall::RESPONSE_SUCCESS) {
            return $states = $stateList['data'];
        }
        return $states;
    }

    function updateGeoLocationData($data)
    {
        $url = UPDATE_CLIENT_BASIC_REG;
        $data = [
            'id' => $data['id'],
            'cr_geo_location_longtitude' => $data['longitude'],
            "cr_biz_name" => $data['lattitude'],
        ];
        return $this->httpWithTokenPost($url, $data);
    }

    function getClientData()
    {
        $url = GET_CLIENT_DATA;
        $profile = [];
        $profileData = $this->getGuzzleRequestWithToken($url);
        $profileData = json_decode($profileData, true);
        if (!empty($profileData)) {
            if ($profileData['status_code'] == ApiCall::RESPONSE_SUCCESS) {
                $profile = $profileData['data'];
            }
        }
        return $profile;
    }


    function addGSTinfo($data)
    {

        $data['chb_state_id'] = 1;
        $data['chb_gst_no'] = "21ABHFM6034A1ZH";
        $data['chb_bank_name'] = "icici";
        $data['chb_account_name'] = "btsicici";
        $data['chb_account_number'] = "1234567890123999";
        $data['chb_ifsc_code'] = "ICIC0000421";
        $data['chb_swift_iban'] = "ICICINBB";
        $data['chb_tax_regn_document'] = "test";
        $url = ADD_BANK_INFO;
        $data = [
            'chb_state_id' => $data['chb_state_id'],
            'chb_gst_no' => $data['chb_gst_no'],
            'chb_bank_name' => $data['chb_bank_name'],
            'chb_account_name' => $data['chb_account_name'],
            'chb_account_number' => $data['chb_account_number'],
            'chb_ifsc_code' => $data['chb_ifsc_code'],
            'chb_swift_iban' => $data['chb_swift_iban'],
            'chb_tax_regn_document' => $data['chb_tax_regn_document']
        ];
        return $this->httpWithTokenPost($url, $data);
    }

    function postFeedback($data)
    {
        $url = POST_FEEDBACK;
        $data = ['sc_type' => $data['sc_type'],
            'sc_detail' => $data['sc_detail'],
        ];
        return $this->postGuzzleRequestWithToken($url, $data);

    }

    function getFeedback()
    {
        $url = GET_FEEDBACK;
        return $this->getGuzzleRequestWithToken($url);
    }


    public function getGuzzleRequest($url)
    {
        $url = BASE_URL . $url;
        $client = new \GuzzleHttp\Client();
        $request = $client->get($url);
        $response = $request->getBody()->getContents();

        return $response;
    }

    public function postGuzzleRequest()
    {
        $client = new \GuzzleHttp\Client();
        $url = "http://myexample.com/api/posts";

        $myBody['name'] = "Demo";
        $request = $client->post($url, ['body' => $myBody]);
        $response = $request->send();

        dd($response);
    }

    function getGuzzleRequestWithToken($url)
    {
        $url = BASE_URL . $url;
        $token = $this->getToken();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];
        $client = new Client();
        $response = $client->get($url, $headers);
        $response = $response->getBody()->getContents();
        return $response;
        /*try {

        } catch (RequestException $e) {
            return $e->getMessage();
        }*/

    }

    function postGuzzleRequestWithToken($url, $data)
    {
        $client = new Client();
        $url = BASE_URL . $url;
        $token = $this->getToken();
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];
        $request = $client->post($url, ['headers' => $headers, 'form_params' => $data]);
        return $request->getBody()->getContents();
    }
}

